<?php

class Reservation_model extends CI_model {
	public function getRooms()
	{
		$this->db->select('COUNT(rooms.room_type_id) as num_room, room_type.name, room_type.base_price, rooms.room_type_id');
		$this->db->from('rooms');
		$this->db->join('room_type', 'rooms.room_type_id = room_type.room_type_id');
		$this->db->group_by('rooms.room_type_id');
		$query = $this->db->get();
		return $query->result();
	}

	public function getAvailableRooms($check_in_date, $check_out_date)
	{
		$rooms = $this->getRooms();
		foreach ($rooms as $key => $room) {
			$reserved_rooms = $this->getReservedRooms($check_in_date, $check_out_date, $room->room_type_id);
			$rooms[$key]->num_room -= count($reserved_rooms);
		}
		return $rooms;
	}

	public function getReservedRooms($check_in_date, $check_out_date, $room_type_id)
	{
		$check_in_date = date("Y-m-d", strtotime($check_in_date));
		$check_out_date = date("Y-m-d", strtotime($check_out_date));
		$this->db->from('reserved_rooms');
		$this->db->where("((date_in <= '$check_in_date' AND date_out > '$check_in_date') OR ( date_in < '$check_out_date' AND date_out > '$check_out_date') OR (date_in >= '$check_in_date' AND date_out <= '$check_out_date'))");
		$this->db->where("room_type_id = $room_type_id");
		$this->db->where("(status = 'paid' OR status = 'checked-in' OR status = 'pending')");
		$this->db->group_by('reserved_room_id');
		$query = $this->db->get();
		return $query->result();
	}

	public function insertReservation($post)
	{
		$data = $post;
		unset($data['number_of_rooms']);
		$data['status'] = "paid";
		// var_dump($post);die();
		for ($i=0; $i < $post['number_of_rooms']; $i++) { 
			$this->db->insert('reserved_rooms', $data);
		}
	}
}

?>