
<!--sidebar start-->
<aside>
  <div id="sidebar"  class="nav-collapse ">
    <!-- sidebar menu start-->
    <ul class="sidebar-menu" id="nav-accordion">
      <li>
        <a href="<?php echo base_url('admin/dashboard'); ?>">
          <i class="fa fa-dashboard"></i>
          <span>Dashboard</span>
        </a>
      </li>

      <li>
        <a href="<?php echo base_url('admin/rooms'); ?>">
          <i class="fa fa-dashboard"></i>
          <span>Rooms</span>
        </a>
      </li>

      <li>
        <a href="<?php echo base_url('admin/roomTypes'); ?>">
          <i class="fa fa-dashboard"></i>
          <span>Room Types</span>
        </a>
      </li>

      <li>
        <a href="<?php echo base_url('admin/inclusions'); ?>">
          <i class="fa fa-dashboard"></i>
          <span>Inclusions</span>
        </a>
      </li>

    </ul>
    <!-- sidebar menu end-->
  </div>
</aside>
<!--sidebar end-->