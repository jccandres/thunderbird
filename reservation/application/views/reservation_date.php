<link href="<?php echo base_url('assets/css/custom-date-picker.css'); ?>" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/skeleton/2.0.4/skeleton.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.structure.min.css">


<div class="container">
  <div id="calendar-details">
    <div class="check-in">
      <h5>Check-In</h5>
      <h6 id="check-in-date">Choose a date</h6>
    </div>
    <div class="arrow"></div>
    <div class="check-out">
      <h5>Check-Out</h5>
      <h6 id="check-out-date">Choose a date</h6>
    </div>
  </div>
  <div id="calendar"></div>

  <button id="btn-next">Next</button>

</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="<?php echo base_url('assets/js/custom_js/custom-date-picker.js'); ?>"></script>

<script type="text/javascript">
$(function(){
	$("#btn-next").on("click", function() {
		var check_in_date = $("#check-in-date").text();
		var check_out_date = $("#check-out-date").text();
		if (check_in_date == "Choose a date" && check_out_date == "Choose a date") {
			alert("Choose a date");
		}
		else {
			window.location.href = "<?php echo base_url(); ?>selectRoom/" + check_in_date + "/" + check_out_date;
		}
	});
});
</script>