<?php
// var_dump($room_type); die();
?>
<!--main content start-->
<section id="main-content">
	<section class="wrapper site-min-height">
		<!-- page start-->

<div class="row">
	<div class="col-lg-12">
		<section class="panel">
			<header class="panel-heading">
                Room Availability
            </header>
            
            <div class="panel-body">
	            <div class="col-md-3 mbot15">
	            	<input class="form-control form-control-inline input-medium default-date-picker"  size="16" type="text" onchange="window.location.href = '<?php echo base_url(uri_string()); ?>/?date='+$(this).val();" value="<?php echo $date; ?>">
	            </div>
                <div class="col-md-3 mbot15">
                    <a href="#EditRoomModal" data-toggle="modal" class="btn btn-default">Edit Number of Rooms</a>
                </div>
            	<div class="table-responsive col-md-12">
            		<table class="table table-bordered">
            			<thead>
            				<tr>
                            	<th>Room type</th>
                            	<?php
                            	for ($i=0; $i < 10; $i++) { 
                            	?>

                            	<th>
                            		<?php
                            		echo date("D, d M", strtotime("+$i days", strtotime($date)));
                            		?>
                            	</th>

                            	<?php
                            	}
                            	?>
                            </tr>
            			</thead>
            			<tbody>
                            <?php
                            foreach ($rooms as $key => $room) {
                            ?>

                            <tr>
                                <td><?php echo $key; ?></td>
                                <?php
                                foreach ($room as $val) {
                                ?>

                                <td><?php echo $val; ?></td>

                                <?php
                                }
                                ?>
                            </tr>

                            <?php
                            }
                            ?>  
            			</tbody>
            		</table>
            	</div>
            </div>
		</section>
	</div>
</div>

		<!-- page end-->
	</section>
</section>
<!--main content end-->


<!-- Modals -->
    
    <!-- Edit Number of Rooms Modal -->
    <div class="modal fade " id="EditRoomModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit Number of Rooms</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">

                            <!-- Select -->
                            <div id="selectRoom">
                            <div class="row form-group">
                                <label class="col-md-2 control-label" for="inputSuccess">Room Type</label>
                                <div class="col-md-10">
                                    <select class="form-control m-bot15 roomSelect" name="room_type_id">
                                        <option value="">Select Room Type</option>
                                        <?php
                                        foreach ($room_type as $key => $room) {
                                        ?>

                                        <option value="<?php echo $room->room_type_id; ?>"><?php echo $room->name; ?></option>

                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            </div>
                            <!-- Select -->

                            <!--  -->
                            <div id="rooms" style="display: none;">
                                
                            </div>
                            <!--  -->

                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <a href="javascript:void(0);" class="btn btn-success btn-xs add-room-btn" style="display: none;">add room</a>
                </div>
                
            </div>
        </div>
    </div>
    <!-- Edit Number of Rooms Modal -->


<!-- Modals END -->

<script type="text/javascript">
    $(function(){
        var base_url = "<?php echo base_url(); ?>";
        $(".roomSelect").on("change", function(){
            var room_type_id = $(".roomSelect").val();
            $.ajax({
                url: base_url + "admin/getRooms/" + room_type_id,
                type: 'POST',
                success: function(data) {
                    displayRooms(data);
                }
            });
        });

        $(".add-room-btn").on("click", function(){
            var room_type_id = $(".roomSelect").val();
            $.ajax({
                url: base_url + "admin/addRoom/" + room_type_id,
                type: 'POST',
                success: function(data) {
                    displayRooms(data);
                }
            });
        });

        $("body").on("click", ".delete-room-btn", function(){
            if (confirm("Are you sure you want to do this?")) {
                var room_id = $(this).data('id');
                var room_type_id = $(".roomSelect").val();
                 $.ajax({
                    url: base_url + "admin/deleteRoom/" + room_type_id + "/" + room_id,
                    success: function(data) {
                        displayRooms(data);
                    }
                });
            }
        });

        $("body").on("click", ".update-room-btn", function(){
            
            var room_type_id = $(".roomSelect").val();
            var room_id = $(this).data('id');
            var room_num = $("#room_num_"+room_id).val();
            $.ajax({
                url: base_url + "admin/updateRoom/" + room_type_id + "/" + room_id + "/" + room_num,
                success: function(data) {
                    displayRooms(data);
                }
            });
            
        });

        displayRooms = function(data) {
            $(".add-room-btn").hide();
            $("#rooms").empty();
            $("#rooms").hide();
            var result = "";
            var rooms = jQuery.parseJSON(data);
            for (var x in rooms) {
                
                result += `
                <div class="row form-group">
                    <div class="col-md-10">
                        <input type="hidden" value="`+rooms[x].room_id+`" name="room_id[]">
                        <input type="text" 
                            value="`+rooms[x].room_number+`" 
                            class="form-control"
                            id="room_num_`+rooms[x].room_id+`"
                            placeholder="Room no./name">
                    </div>
                    <div class="col-md-2">
                        <a href="javascript:void(0);" 
                            data-id="`+rooms[x].room_id+`"
                            class="btn btn-info btn-xs update-room-btn">
                                <i class="fa fa-check"></i>
                        </a>
                        <a href="javascript:void(0);" 
                            data-id="`+rooms[x].room_id+`" 
                            class="btn btn-danger btn-xs delete-room-btn">
                                <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                `;
            }
            
            $("#rooms").append(result);
            $("#rooms").show(300);
            $(".add-room-btn").show();
        }
    });

</script>
