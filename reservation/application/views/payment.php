Payment Form
<form name="payFormCard" method="post" action="https://test.paydollar.com/ECN/eng/payment/payForm.jsp">
	<input type="hidden" name="merchantId" value="100000375">
	<input type="hidden" name="amount" value="1.00">
	<input type="hidden" name="orderRef" value="000000000014">
	<input type="hidden" name="currCode" value="608"> <!-- 608 = PHP; 840 = USD; -->
	<input type="hidden" name="successUrl" value="http://localhost/thunderbird/reservation/payment/success">
	<input type="hidden" name="failUrl" value="http://localhost/thunderbird/reservation/payment/fail">
	<input type="hidden" name="cancelUrl" value="http://localhost/thunderbird/reservation/payment/cancel">
	<input type="hidden" name="payType" value="N">
	<input type="hidden" name="lang" value="E">
	<input type="submit" name="" value="Pay">
</form>