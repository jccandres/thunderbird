<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class admin extends CI_Controller {

	public function __construct() 
	{
        parent::__construct();
        $this->load->model('reservation_model');
        $this->load->model('room_model');
        $this->load->model('inclusion_model');
    }

	public function wrapper($body, $data = NULL) 
	{
		if (isset($this->session->userdata['logged_in'])) {
			$this->load->view('partials/header');
			$this->load->view('partials/left-sidebar');
			$this->load->view($body, $data);
			// $this->load->view('partials/right-sidebar');
			$this->load->view('partials/footer');
		}
		else {
			redirect(base_url('admin'));
		}
	}
	
	public function index() 
	{
		if (isset($this->session->userdata['logged_in'])) {
			$this->dashboard();
		}
		else {
			$this->load->view('admin/login');
		}
	}

	public function login()
	{
		$this->session->userdata['logged_in'] = 1;
		redirect(base_url('admin'));
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url('admin'));
	}

	public function dashboard()
	{
		$this->wrapper('admin/dashboard');
	}

/* Rooms Start*/
	public function rooms()
	{
		if (isset($_GET['date'])) {
			$data['date'] = $_GET['date'];
		}
		else {
			$data['date'] = date("d-m-Y");
		}

		/* Get available rooms for 10 days */
		for ($i=0; $i < 10; $i++) {
			$date = date("d-m-Y", strtotime("+$i days", strtotime($data['date'])));
			$available_rooms = $this->reservation_model->getAvailableRooms($date, $date);

			foreach ($available_rooms as $value) {
				$data['rooms'][$value->name][] = $value->num_room;
			}
		}
		/* Get available rooms for 10 days */

		// $data['room_type'] = $this->reservation_model->getRooms();
		$data['room_type'] = $this->room_model->getRoomTypes();

		// var_dump($data); die();

		$this->wrapper('admin/rooms', $data);
	}

	public function getRooms($room_type_id) 
	{
		echo json_encode($this->room_model->getRooms($room_type_id));
	}

	public function addRoom($room_type_id) 
	{
		if ($this->room_model->addRoom($room_type_id)) {
			echo json_encode($this->room_model->getRooms($room_type_id));
		}
	}

	public function updateRoom($room_type_id, $room_id, $room_num)
	{
		if ($this->room_model->updateRoom($room_id, $room_num)) {
			echo json_encode($this->room_model->getRooms($room_type_id));
		}
	}

	public function deleteRoom($room_type_id, $room_id) 
	{
		if ($this->room_model->deleteRoom($room_id)) {
			echo json_encode($this->room_model->getRooms($room_type_id));
		}
	}
/* Rooms END */

/* Room Types START */
	public function roomTypes()
	{
		$data['roomTypes'] = $this->room_model->getRoomTypes();
		$data['inclusions'] = $this->inclusion_model->getInclusions();
		if (isset($_GET['dev'])) {
			$data['dev'] = true;
		}
		$this->wrapper('admin/room_types', $data);
	}

	public function addRoomType()
	{
		$data = $this->input->post();
		// var_dump($data); die();
		if ($this->room_model->addRoomType($data)) {
			$msg_data = array('alert_msg' => 'Add Success', 'alert_color' => 'green');
		}
		else {
			$msg_data = array('alert_msg' => 'Add Fail', 'alert_color' => 'red');
		}
		$this->session->set_flashdata($msg_data);
		redirect(base_url('admin/roomTypes'));
	}

	public function editRoomType()
	{
		$data = $this->input->post();
		if ($this->room_model->updateRoomType($data)) {
			$msg_data = array('alert_msg' => 'Edit Success', 'alert_color' => 'green');
		}
		else {
			$msg_data = array('alert_msg' => 'Edit Fail', 'alert_color' => 'red');
		}
		$this->session->set_flashdata($msg_data);
		redirect(base_url('admin/roomTypes'));
	}

	public function deleteRoomType($id)
	{
		if ($this->room_model->deleteRoomType($id)) {
			$msg_data = array('alert_msg' => 'Delete Success', 'alert_color' => 'green');
		}
		else {
			$msg_data = array('alert_msg' => 'Delete Fail', 'alert_color' => 'red');
		}
		$this->session->set_flashdata($msg_data);
		redirect(base_url('admin/roomTypes'));
	}
/* Room Types END */

/* Inclusions START */
	public function inclusions()
	{
		$data['inclusions'] = $this->inclusion_model->getInclusions();
		if (isset($_GET['dev'])) {
			$data['dev'] = true;
		}
		$this->wrapper('admin/inclusions', $data);
	}

	public function addInclusion()
	{
		$data = $this->input->post();
		if ($this->inclusion_model->addInclusion($data)) {
			$msg_data = array('alert_msg' => 'Add Success', 'alert_color' => 'green');
		}
		else {
			$msg_data = array('alert_msg' => 'Add Fail', 'alert_color' => 'red');
		}
		$this->session->set_flashdata($msg_data);
		redirect(base_url('admin/inclusions'));
	}

	public function editInclusion()
	{
		$data = $this->input->post();
		if ($this->inclusion_model->updateInclusion($data)) {
			$msg_data = array('alert_msg' => 'Update Success', 'alert_color' => 'green');
		}
		else {
			$msg_data = array('alert_msg' => 'Update Fail', 'alert_color' => 'red');
		}
		$this->session->set_flashdata($msg_data);
		redirect(base_url('admin/inclusions'));
	}

	public function deleteInclusion($id)
	{
		if ($this->inclusion_model->deleteInclusion($id)) {
			$msg_data = array('alert_msg' => 'Delete Success', 'alert_color' => 'green');
		}
		else {
			$msg_data = array('alert_msg' => 'Delete Fail', 'alert_color' => 'red');
		}
		$this->session->set_flashdata($msg_data);
		redirect(base_url('admin/inclusions'));
	}
/* Inclusions END */

}