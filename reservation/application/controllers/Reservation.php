<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class reservation extends CI_Controller {

	public function __construct() 
	{
        parent::__construct();
        $this->load->model('reservation_model');
    }

    public function index()
    {
    	$this->load->view('reservation_date');
    }
    
    public function selectRoom($check_in_date, $check_out_date)
    {
    	$data['rooms'] = $this->reservation_model->getAvailableRooms($check_in_date, $check_out_date);
    	$data['check_in_date'] = $check_in_date;
    	$data['check_out_date'] = $check_out_date;
    	$this->load->view('room_selection', $data);
    }

    public function reserve()
    {
    	$number_of_rooms = $this->input->post('number_of_rooms');
    	$price = $this->input->post('price');
    	$room_type_id = $this->input->post('room_type_id');
    	$check_in_date = $this->input->post('check_in_date');
    	$check_out_date = $this->input->post('check_out_date');
    	for ($i=0; $i < count($number_of_rooms); $i++) { 
    		if ($number_of_rooms[$i] != "") {
    			$post['number_of_rooms'] = $number_of_rooms[$i];
    			$post['price'] = $price[$i];
    			$post['room_type_id'] = $room_type_id[$i];
    			$post['date_in'] = date("Y-m-d", strtotime($check_in_date[$i]));
    			$post['date_out'] = date("Y-m-d", strtotime($check_out_date[$i]));
                $temp['data'] = $post;
                $this->load->view('review_reservation', $temp);
    			// $this->reservation_model->insertReservation($post);
    		}
    	}
    	// redirect("reservation");
    }

    public function pay()
    {
        $this->load->view('payment');
    }
}