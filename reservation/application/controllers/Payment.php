<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class payment extends CI_Controller {

	public function __construct() 
	{
        parent::__construct();
        
    }

    public function index()
    {
    	
    }

    public function success()
    {
    	$this->load->view("payment_success");
    }

    public function fail()
    {
    	$this->load->view('payment_fail');
    }

    public function cancel()
    {
    	$this->load->view('payment_cancel');
    }
}