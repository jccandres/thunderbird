<?php
/* Template Name: Rizal Promo */
get_header("rizal");
while(have_posts()): the_post();
?>

<div class="first-half">
	<div class="welcome">

		<?php get_template_part( 'template-parts/navigation/rizal/nav', 'menu' ); ?>

		<img src="<?php echo get_field('header_image'); ?>" alt="">

		<?php get_template_part( 'template-parts/navigation/rizal/nav', 'reservation' ); ?>

	</div>

	<div class="banners">
		<div class="container">
			<div class="title">
				<h2><?php echo get_field('header'); ?></h2>
				<?php echo apply_filters('the_content', get_post_field('post_content')); ?>
			</div>
			<div class="list">
				<?php
				$ctr = 0;
				foreach (get_field('promos') as $promo) {
				?>

				<div class="item">
					<h3><?php echo $promo['title']; ?></h3>
					<div class="row">
						<div class="image col-lg-8 col-md-8 col-sm-7 col-xs-12">
							<a href="<?php echo $promo['image'] ?>" class="image-popup"><img src="<?php echo $promo['image']; ?>" alt=""></a>
						</div>
						<div class="link col-lg-4 col-md-4 col-sm-5 col-xs-12">
							<a href="#details-<?php echo $ctr; ?>" class="inline-popup">view details</a>
						</div>
					</div>
				</div>

				<div id="details-<?php echo $ctr++; ?>" class="white-popup mfp-hide">
					<?php echo $promo['details']; ?>
				</div>

				<?php
				}
				?>

			</div>
		</div>
	</div>

</div>


<?php 
endwhile;
get_footer("rizal");
?>