jQuery(document).ready(function() {
    new Swiper(".top-slider", {
        slidesPerView: 1,
        loop: !0,
        navigation: {
            nextEl: ".welcome .swiper-button-next",
            prevEl: ".welcome .swiper-button-prev"
        },
        pagination: {
            el: ".top-slider .swiper-pagination",
            clickable: !0
        }
    }), new Swiper(".gallery-first", {
        slidesPerView: 3,
        loop: !0,
        spaceBetween: 0,
        centeredSlides: true,
        navigation: {
            nextEl: ".gallery .inner.first .swiper-button-next",
            prevEl: ".gallery .inner.first .swiper-button-prev"
        },
        breakpoints: {
            960: {
                slidesPerView: 1
            }
        }
    }), new Swiper(".gallery-second", {
        slidesPerView: 3,
        loop: !0,
        spaceBetween: 0,
        navigation: {
            nextEl: ".gallery .inner.second .swiper-button-next",
            prevEl: ".gallery .inner.second .swiper-button-prev"
        },
        breakpoints: {
            960: {
                slidesPerView: 1
            }
        }
    }), new Swiper(".promo-slider", {
        slidesPerView: 3,
        spaceBetween: 0,
        scrollbar: {
            el: ".promo .swiper-scrollbar",
            draggable: !0,
            dragSize: "51",
            hide: !1
        },
        breakpoints: {
            767: {
                slidesPerView: 1
            }
        }
    });
    jQuery("a.top").click(function(e) {
        e.preventDefault(), jQuery("html, body").animate({
            scrollTop: 0
        }, 600)
    }), jQuery(".hamb").click(function(e) {
        e.preventDefault(), jQuery(".header__nav").hasClass("opened") ? (jQuery(".header__nav").removeClass("opened"), jQuery(".header__nav").slideUp(400)) : (jQuery(".header__nav").addClass("opened"), jQuery(".header__nav").slideDown(400))
    })
}), jQuery(window).on("load", function() {
    jQuery("#status").fadeOut(), jQuery("#preloader").delay(350).fadeOut("slow"), jQuery("body").delay(350).css({
        overflow: "visible"
    })
});