jQuery(document).ready(function() {
    new Swiper(".top-slider", {
        slidesPerView:1, loop:!0, navigation: {
            nextEl: ".welcome .swiper-button-next", prevEl: ".welcome .swiper-button-prev"
        }
        , pagination: {
            el: ".top-slider .swiper-pagination", clickable: !0
        }
    }
    ), new Swiper(".gallery-first", {
        slidesPerView:3, centeredSlides:true, loop:!0, spaceBetween:0, navigation: {
            nextEl: ".gallery .inner.first .swiper-button-next", prevEl: ".gallery .inner.first .swiper-button-prev"
        }
        , breakpoints: {
            1200: {
                slidesPerView: 1
            }
        }
    }
    ), new Swiper(".gallery-second", {
        slidesPerView:2, loop:!0, spaceBetween:0, navigation: {
            nextEl: ".gallery .inner.second .swiper-button-next", prevEl: ".gallery .inner.second .swiper-button-prev"
        }
        , breakpoints: {
            1200: {
                slidesPerView: 1
            }
        }
    }
    ), new Swiper(".events-slider", {
        slidesPerView:2, spaceBetween:0, scrollbar: {
            el: ".promo.casino-page .swiper-scrollbar", draggable: !0, dragSize: "51", hide: !1
        }
        , breakpoints: {
            767: {
                slidesPerView: 1
            }
        }
    }
    ), new Swiper(".promo-slider", {
        slidesPerView:3, spaceBetween:0, scrollbar: {
            el: ".promo.right .swiper-scrollbar", draggable: !0, dragSize: "51", hide: !1
        }
        , breakpoints: {
            767: {
                slidesPerView: 1
            }
        }
    }
    ), new Swiper(".promo-slider2", {
        slidesPerView:3, spaceBetween:0, initialSlide:6, scrollbar: {
            el: ".promo.left .swiper-scrollbar", draggable: !0, dragSize: "51", hide: !1
        }
        , breakpoints: {
            767: {
                slidesPerView: 1
            }
        }
    }
    ), new Swiper(".rest-first", {
        slidesPerView:3, loop:!0, spaceBetween:0, navigation: {
            nextEl: ".restaurant .item.first .swiper-button-next", prevEl: ".restaurant .item.first .swiper-button-prev"
        }
        , breakpoints: {
            992: {
                slidesPerView: 2
            }
            , 767: {
                slidesPerView: 1
            }
        }
    }
    ), new Swiper(".rest-second", {
        slidesPerView:3, loop:!0, spaceBetween:0, navigation: {
            nextEl: ".restaurant .item.second .swiper-button-next", prevEl: ".restaurant .item.second .swiper-button-prev"
        }
        , breakpoints: {
            992: {
                slidesPerView: 2
            }
            , 767: {
                slidesPerView: 1
            }
        }
    }
    ), new Swiper(".rest-third", {
        slidesPerView:3, loop:!0, spaceBetween:0, navigation: {
            nextEl: ".restaurant .item.third .swiper-button-next", prevEl: ".restaurant .item.third .swiper-button-prev"
        }
        , breakpoints: {
            992: {
                slidesPerView: 2
            }
            , 767: {
                slidesPerView: 1
            }
        }
    }
    );
    jQuery("a.top").click(function(e) {
        e.preventDefault(), jQuery("html, body").animate( {
            scrollTop: 0
        }
        , 600)
    }
    ), jQuery(".hamb").click(function(e) {
        e.preventDefault(), jQuery(".header__nav").hasClass("opened")?(jQuery(".header__nav").removeClass("opened"), jQuery(".header__nav").slideUp(400)): (jQuery(".header__nav").addClass("opened"), jQuery(".header__nav").slideDown(400))
    }
    )
}

),
jQuery(window).on("load", function() {
    jQuery("#status").fadeOut(), jQuery("#preloader").delay(350).fadeOut("slow"), jQuery("body").delay(350).css( {
        overflow: "visible"
    }
    )
}

);