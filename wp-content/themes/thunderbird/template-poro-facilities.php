<?php
/* Template Name: Poro Point Facilities */
get_header("poro");
while(have_posts()): the_post();
// var_dump(get_field('facilities')); die();
?>

<div class="first-half">
	<div class="welcome">
		
		<?php get_template_part( 'template-parts/navigation/poro-point/nav', 'menu' ); ?>

		<img src="<?php echo get_field('header_image'); ?>" alt="">

		<?php get_template_part( 'template-parts/navigation/poro-point/nav', 'reservation' ); ?>

	</div>

	<div class="facilities">
		<div class="container">
			<div class="title">
				<h2><?php echo get_field('header') ?></h2>
				<?php echo apply_filters('the_content', get_post_field('post_content')); ?>
			</div>
		</div>
		<?php
		foreach (get_field('facilities') as $value) {
		?>

		<!-- <div class="promo custom right">
			<div class="container">
				<div class="row">
					<div class="text col-lg-3 col-md-3 col-sm-4 col-xs-12">
						<h3>Satisfying Moments</h3>
						<p>Olives Restaurant</p>
					</div>
					<div class="images col-lg-9 col-md-9 col-sm-8 col-xs-12">
						<div class="swiper-container promo-slider">
							<div class="swiper-wrapper">
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image3.jpg" alt="">
									<div class="meta">
										<span>DELICIOUS</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image4.jpg" alt="">
									<div class="meta">
										<span>DELICIOUS</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image3.jpg" alt="">
									<div class="meta">
										<span>DELICIOUS</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image4.jpg" alt="">
									<div class="meta">
										<span>DELICIOUS</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image3.jpg" alt="">
									<div class="meta">
										<span>DELICIOUS</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image4.jpg" alt="">
									<div class="meta">
										<span>DELICIOUS</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image3.jpg" alt="">
									<div class="meta">
										<span>DELICIOUS</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image4.jpg" alt="">
									<div class="meta">
										<span>DELICIOUS</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="swiper-scrollbar"></div>
			</div>
		</div>

		<div class="promo custom left">
			<div class="container">
				<div class="row">
					<div class="images col-lg-9 col-md-9 col-sm-8 col-xs-12">
						<div class="swiper-container promo-slider2">
							<div class="swiper-wrapper">
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image3.jpg" alt="">
									<div class="meta">
										<span>SERVICES</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image4.jpg" alt="">
									<div class="meta">
										<span>SERVICES</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image3.jpg" alt="">
									<div class="meta">
										<span>SERVICES</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image4.jpg" alt="">
									<div class="meta">
										<span>SERVICES</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image3.jpg" alt="">
									<div class="meta">
										<span>SERVICES</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image4.jpg" alt="">
									<div class="meta">
										<span>SERVICES</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image3.jpg" alt="">
									<div class="meta">
										<span>SERVICES</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image4.jpg" alt="">
									<div class="meta">
										<span>SERVICES</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="text col-lg-3 col-md-3 col-sm-4 col-xs-12">
						<h3>Carefree Experiences</h3>
						<p>24-hour room service</p>
					</div>
				</div>
				<div class="swiper-scrollbar"></div>
			</div>
		</div> -->

		<?php
		}
		?>
		<div class="promo custom right">
			<div class="container">
				<div class="row">
					<div class="text col-lg-3 col-md-3 col-sm-4 col-xs-12">
						<h3>Satisfying Moments</h3>
						<p>Olives Restaurant</p>
					</div>
					<div class="images col-lg-9 col-md-9 col-sm-8 col-xs-12">
						<div class="swiper-container promo-slider">
							<div class="swiper-wrapper">
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image3.jpg" alt="">
									<div class="meta">
										<span>DELICIOUS</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image4.jpg" alt="">
									<div class="meta">
										<span>DELICIOUS</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image3.jpg" alt="">
									<div class="meta">
										<span>DELICIOUS</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image4.jpg" alt="">
									<div class="meta">
										<span>DELICIOUS</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image3.jpg" alt="">
									<div class="meta">
										<span>DELICIOUS</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image4.jpg" alt="">
									<div class="meta">
										<span>DELICIOUS</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image3.jpg" alt="">
									<div class="meta">
										<span>DELICIOUS</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image4.jpg" alt="">
									<div class="meta">
										<span>DELICIOUS</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="swiper-scrollbar"></div>
			</div>
		</div>

		<div class="promo custom left">
			<div class="container">
				<div class="row">
					<div class="images col-lg-9 col-md-9 col-sm-8 col-xs-12">
						<div class="swiper-container promo-slider2">
							<div class="swiper-wrapper">
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image3.jpg" alt="">
									<div class="meta">
										<span>SERVICES</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image4.jpg" alt="">
									<div class="meta">
										<span>SERVICES</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image3.jpg" alt="">
									<div class="meta">
										<span>SERVICES</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image4.jpg" alt="">
									<div class="meta">
										<span>SERVICES</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image3.jpg" alt="">
									<div class="meta">
										<span>SERVICES</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image4.jpg" alt="">
									<div class="meta">
										<span>SERVICES</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image3.jpg" alt="">
									<div class="meta">
										<span>SERVICES</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image4.jpg" alt="">
									<div class="meta">
										<span>SERVICES</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="text col-lg-3 col-md-3 col-sm-4 col-xs-12">
						<h3>Carefree Experiences</h3>
						<p>24-hour room service</p>
					</div>
				</div>
				<div class="swiper-scrollbar"></div>
			</div>
		</div>

		<div class="promo custom right">
			<div class="container">
				<div class="row">
					<div class="text col-lg-3 col-md-3 col-sm-4 col-xs-12">
						<h3>Satisfying Moments</h3>
						<p>Olives Restaurant</p>
					</div>
					<div class="images col-lg-9 col-md-9 col-sm-8 col-xs-12">
						<div class="swiper-container promo-slider">
							<div class="swiper-wrapper">
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image3.jpg" alt="">
									<div class="meta">
										<span>DELICIOUS</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image4.jpg" alt="">
									<div class="meta">
										<span>DELICIOUS</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image3.jpg" alt="">
									<div class="meta">
										<span>DELICIOUS</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image4.jpg" alt="">
									<div class="meta">
										<span>DELICIOUS</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image3.jpg" alt="">
									<div class="meta">
										<span>DELICIOUS</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image4.jpg" alt="">
									<div class="meta">
										<span>DELICIOUS</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image3.jpg" alt="">
									<div class="meta">
										<span>DELICIOUS</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image4.jpg" alt="">
									<div class="meta">
										<span>DELICIOUS</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="swiper-scrollbar"></div>
			</div>
		</div>

		<div class="promo custom left">
			<div class="container">
				<div class="row">
					<div class="images col-lg-9 col-md-9 col-sm-8 col-xs-12">
						<div class="swiper-container promo-slider2">
							<div class="swiper-wrapper">
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image3.jpg" alt="">
									<div class="meta">
										<span>SERVICES</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image4.jpg" alt="">
									<div class="meta">
										<span>SERVICES</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image3.jpg" alt="">
									<div class="meta">
										<span>SERVICES</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image4.jpg" alt="">
									<div class="meta">
										<span>SERVICES</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image3.jpg" alt="">
									<div class="meta">
										<span>SERVICES</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image4.jpg" alt="">
									<div class="meta">
										<span>SERVICES</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image3.jpg" alt="">
									<div class="meta">
										<span>SERVICES</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
								<div class="swiper-slide">
									<img src="<?php bloginfo("template_url"); ?>/assets/img/promo-image4.jpg" alt="">
									<div class="meta">
										<span>SERVICES</span><br>
										<a href="#">LEARN MORE</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="text col-lg-3 col-md-3 col-sm-4 col-xs-12">
						<h3>Carefree Experiences</h3>
						<p>24-hour room service</p>
					</div>
				</div>
				<div class="swiper-scrollbar"></div>
			</div>
		</div>

	</div>
</div>

<?php 
endwhile;
get_footer("poro");
?>