<?php
/**
 * Footer Template for poro point
 */
?>
	<div class="second-half">
	<footer class="footer">
		<div class="container">
			<a href="#" class="top"><img src="<?php bloginfo("template_url"); ?>/assets/img/top.png" alt=""></a>
			<div class="row">
				<div class="footer__text col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<?php echo get_field('poro_contact_info', 88); ?>
					<a href="tel:<?php echo get_field('poro_contact_no', 88); ?>" class="tel">
						<img src="<?php bloginfo("template_url"); ?>/assets/img/icon-tel.png" alt=""><?php echo get_field('contact_no', 88); ?>
					</a>
					<br>
					<a href="mailto:<?php echo get_field('poro_contact_email', 88); ?>" class="email"><img src="<?php bloginfo("template_url"); ?>/assets/img/icon-email.png" alt=""><?php echo get_field('poro_contact_email', 88); ?></a>
					<div class="social">
						<a href="<?php echo get_field('poro_facebook_link', 88); ?>">
							<img src="<?php bloginfo("template_url"); ?>/assets/img/fb.png" alt="">
						</a>
						<a href="<?php echo get_field('poro_twitter_link', 88); ?>">
							<img src="<?php bloginfo("template_url"); ?>/assets/img/tw.png" alt="">
						</a>
						<a href="<?php echo get_field('poro_instagram_link', 88); ?>">
							<img src="<?php bloginfo("template_url"); ?>/assets/img/insta.png" alt="">
						</a>
					</div>
				</div>
				<div class="footer__subscribe col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<form>
						<input type="email" name="email" class="email" placeholder="" required>
						<input type="submit" value="Subscribe" name="subscribe">
					</form>
					<div class="span2" style="padding-top:25px">
						<a href="http://www.doubledowncasino.com/?cid=3959&amp;origin=96" target="_blank"><img src="http://wpcdn.hotelhosting.co/wp-content/uploads/sites/921/2016/04/dd-icon.png" title="Thunderbird Resort Poro Point in La Union, Philippines" alt="Thunderbird Resort Poro Point in La Union, Philippines | undefined"></a><br>
						<strong><img src="http://wpcdn.hotelhosting.co/wp-content/uploads/sites/921/2016/08/icon-pdf.png" alt="Thunderbird Resort Poro Point in La Union, Philippines | " title="Thunderbird Resort Poro Point in La Union, Philippines"> &nbsp; <a href="<?php echo get_field('poro_faq_file', 88); ?>" target="_blank">Responsible Gaming FAQs</a></strong>
					</div>
				</div>
				<div class="footer__logo col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<a href="<?php echo get_permalink(8); ?>"><img src="<?php echo get_field('main_logo', 8); ?>" alt=""></a>
				</div>
			</div>
			<nav class="footer__nav">
				<ul>
					<?php
					$menu_obj = wp_get_nav_menu_items('main-page-poro-point-menu');
					foreach ($menu_obj as $obj) {
					?>

					<li><a href="<?php echo $obj->url ?>"><?php echo get_the_title($obj->object_id); ?></a></li>

					<?php
					}
					?>
				</ul>
			</nav>
			<p>&copy; 2017 Thunderbird-asia.com. All Rights Reserved</p>
		</div>
	</footer>
	</div>

	<!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<?php bloginfo("template_url"); ?>/assets/js/swiper.min.js"></script>
    <!-- <script src="<?php bloginfo("template_url"); ?>/assets/js/scripts.js"></script> -->
    <?php echo get_template_part( 'assets/js/scripts' ); ?>
    <script src="<?php bloginfo('template_url'); ?>/assets/js/jquery.magnific-popup.min.js"></script>
    
    <script type="text/javascript">
    	$(function () {
    		$('.image-popup').magnificPopup({
    			type: 'image',
    			closeBtnInside: false,
    			closeOnContentClick: true,
    			image: {
    				verticalFit: false
    			}
    		});
    		$('.inline-popup').magnificPopup({
    			type: 'inline',
    			closeBtnInside: false
    		});
    	});
    </script>
    </body>
</html>