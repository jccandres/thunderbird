<?php
/**
 * Menu for Poro Point page
 */
?>
<div class="container">
	<header class="header">

		<?php if (!is_page(15)): ?>
			<div class="header__buttons pull-left">
				<a href="#" class="btn">RESERVE NOW!</a>
				<a href="#">PROMO DEALS</a>
			</div>
			<div class="header__contacts pull-right">
				<a href="tel:+63-72-8887777" class="tel">+63-72-8887777</a>
				<div class="social">
					<a href="#"><img src="<?php bloginfo("template_url"); ?>/assets/img/fb.png" alt=""></a>
					<a href="#"><img src="<?php bloginfo("template_url"); ?>/assets/img/tw.png" alt=""></a>
					<a href="#"><img src="<?php bloginfo("template_url"); ?>/assets/img/insta.png" alt=""></a>
				</div>
			</div>
			<div class="clearfix"></div>
		<?php endif ?>
		
		<div class="header__logo">
			<a href="<?php echo get_permalink(15); ?>"><img src="<?php echo get_field('logo', 15); ?>" alt=""></a>
		</div>
		<a href="#" class="hamb"><img src="<?php bloginfo("template_url"); ?>/assets/img/hamb.png" alt=""></a>
		<nav class="header__nav">
			<ul>
				<li class="has-subnav">
					<a href="<?php echo get_permalink(35); ?>">Casino</a>
					<ul class="sub-nav">
						<li><a href="<?php echo get_permalink(45); ?>">Rewards</a></li>
						<li><a href="<?php echo get_permalink(47); ?>">Games</a></li>
						<li><a href="<?php echo get_permalink(50); ?>">Entertaiment</a></li>
						<li><a href="<?php echo get_permalink(52); ?>">Pagcor</a></li>
					</ul>
				</li>
				<li><a href="<?php echo get_permalink(19); ?>">Hotel</a></li>
				<li><a href="<?php echo get_permalink(24); ?>">Restaurants</a></li>
				<li><a href="<?php echo get_permalink(26); ?>">Events place</a></li>
				<li><a href="<?php echo get_permalink(29); ?>">Golf club</a></li>
				<li><a href="<?php echo get_permalink(31); ?>">Facilities</a></li>
				<li><a href="<?php echo get_permalink(33); ?>">Promotions</a></li>
			</ul>
		</nav>
	</header>
</div>