<?php
/**
 * Template for reservation menu
 */
?>

<div class="cta">
	<div class="container">
		<form>
			<h3><span class="check"></span>Best Price Guarantee<a class="tip"></a></h3>
			<div class="control-wrapper">
				<span class="calendar-label">Check In</span>
				<div class="calendar-input">
					<input class="hasDatepicker" id="arrival_date" placeholder="Select Date" name="arrival"  type="text">
				</div>
			</div>
			<div class="control-wrapper">
				<span class="calendar-label">Check Out</span>
				<div class="calendar-input">
					<input class="hasDatepicker" id="departure_date" placeholder="Select Date" name="departure"  type="text">
					<input id="promo_code" name="promo_code" value="" type="text" placeholder="Enter promo code here" autocomplete="off">
				</div>
			</div>
			<div class="control-wrapper">
				<input class="button" type="submit" value="Check Availability and Prices">
			</div>
		</form>
	</div>
</div>