<?php
/**
 * Menu for Main landing page
 */
?>

<header class="header">
	<div class="pagewrapper">
		<div class="header__logo">
			<a href=""><img src="<?php echo get_field('main_logo'); ?>" alt=""></a>
		</div>
		<nav class="header__nav">
			<ul>
				<li class="active"><a href="<?php echo get_permalink(15); ?>">Poro point</a></li>
				<li><a href="<?php echo get_permalink(17); ?>">Rizal</a></li>
				<li><a href="<?php echo get_permalink(86); ?>">About us</a></li>
				<li><a href="<?php echo get_permalink(88); ?>">Contact us</a></li>
			</ul>
		</nav>
	</div>
</header>