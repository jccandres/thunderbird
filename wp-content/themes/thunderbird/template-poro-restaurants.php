<?php
/* Template Name: Poro Point Restaurants */
get_header("poro");
while(have_posts()): the_post();
$ctr = 1;
$featured_restaurants = array();
foreach (get_field('featured_restaurants') as $featured) {
	$featured_restaurants[$ctr++] = $featured['restaurant'];
}
?>
<style type="text/css">
.rest {
	height: 467px;
}
.rest .swiper-wrapper .swiper-slide img {
	width: 100%;
	height: 100%;
	object-fit: cover;
}
.restaurant__gallery a {
	height: 319px;
}
.restaurant__gallery a img {
	height: 100%;
	object-fit: cover;
}
</style>

<div class="first-half">
	<div class="welcome">

		<?php get_template_part( 'template-parts/navigation/poro-point/nav', 'menu' ); ?>

		<img src="<?php echo get_field('header_image'); ?>" alt="">

		<?php get_template_part( 'template-parts/navigation/poro-point/nav', 'reservation' ); ?>

	</div>

	<div class="restaurant">
		<div class="container">
			<div class="title">
				<h2><?php echo get_field('header'); ?></h2>
			</div>

			<!-- Featured Restaurants START -->
			<div class="list">

				<?php if (isset($featured_restaurants[1])): ?>
				<div class="item first">
					<div class="desc">
						<h3><?php echo get_the_title($featured_restaurants[1]); ?></h3>
						<?php echo apply_filters('the_content', get_post_field('post_content', $featured_restaurants[1])); ?>
					</div>
					<div class="photos">
						<div class="swiper-container rest rest-first">
							<div class="swiper-wrapper">
								<?php
								foreach (get_field('images', $featured_restaurants[1]) as $image) {
								?>

								<div class="swiper-slide">
									<img src="<?php echo $image['image']; ?>" alt="">
								</div>

								<?php
								}
								?>
							</div>
						</div>
						<div class="swiper-button-next"></div>
						<div class="swiper-button-prev"></div>
					</div>
				</div>
				<?php endif ?>
				
				<?php if (isset($featured_restaurants[2])): ?>
				<div class="item second">
					<div class="desc">
						<h3><?php echo get_the_title($featured_restaurants[2]); ?></h3>
						<?php echo apply_filters('the_content', get_post_field('post_content', $featured_restaurants[2])); ?>
					</div>
					<div class="photos">
						<div class="swiper-container rest rest-second">
							<div class="swiper-wrapper">
								<?php
								foreach (get_field('images', $featured_restaurants[2]) as $image) {
								?>

								<div class="swiper-slide">
									<img src="<?php echo $image['image']; ?>" alt="">
								</div>

								<?php
								}
								?>
							</div>
						</div>
						<div class="swiper-button-next"></div>
						<div class="swiper-button-prev"></div>
					</div>
				</div>
				<?php endif ?>

				<?php if (isset($featured_restaurants[3])): ?>
				<div class="item third">
					<div class="desc">
						<h3><?php echo get_the_title($featured_restaurants[3]); ?></h3>
						<?php echo apply_filters('the_content', get_post_field('post_content', $featured_restaurants[3])); ?>
					</div>
					<div class="photos">
						<div class="swiper-container rest rest-third">
							<div class="swiper-wrapper">
								<?php
								foreach (get_field('images', $featured_restaurants[3]) as $image) {
								?>

								<div class="swiper-slide">
									<img src="<?php echo $image['image']; ?>" alt="">
								</div>

								<?php
								}
								?>
							</div>
						</div>
						<div class="swiper-button-next"></div>
						<div class="swiper-button-prev"></div>
					</div>
				</div>
				<?php endif ?>

			</div>
			<!-- Featured Restaurants END -->

			<div class="restaurant__gallery">

				<?php
				$args = array('post_type' => 'poro_restaurant');
				$the_query = new WP_Query($args);
				if ( $the_query->have_posts() ) {  while ( $the_query->have_posts() ): $the_query->the_post(); ?>

				<a href="<?php echo get_permalink(get_the_id()); ?>"><img src="<?php echo get_the_post_thumbnail_url(); ?>" alt=""></a>

				<?php endwhile; wp_reset_postdata(); } else { /** no posts found **/ } ?>
				
				<a href="#"><img src="<?php bloginfo("template_url"); ?>/assets/img/rest-banner-image4.png" alt=""></a>
			</div>
		</div>
	</div>

</div>


<?php 
endwhile;
get_footer("poro");
?>