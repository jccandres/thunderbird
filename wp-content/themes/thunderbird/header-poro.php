<?php
/**
 * Header template for poro point
 */
?>


<!DOCTYPE HTML>
<html lang="en">
    <head>
        <title>Thunderbird - Poro Point</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" href="<?php bloginfo("template_url"); ?>/assets/css/bootstrap-grid.min.css" />
        <link rel="stylesheet" href="<?php bloginfo("template_url"); ?>/assets/css/swiper.min.css" />
        
        <?php if (is_page(15)): ?>
        <link rel="stylesheet" href="<?php bloginfo("template_url"); ?>/assets/css/style-home.css" />
    	<?php else: ?>
    	<link rel="stylesheet" href="<?php bloginfo("template_url"); ?>/assets/css/style.css" />
        <?php endif ?>
        
        <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/magnific-popup.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0" />
    </head>
    <body class="inner_page">