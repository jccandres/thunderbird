<?php
/**
 * Footer for main landing page
 */
?>


		<footer class="footer">
			<div class="container">
				<p>&copy; 2017 Thunderbird-asia.com. All Rights Reserved</p>
			</div>
		</footer>

		<!-- Preloader -->
		<div id="preloader">
		  <div id="status">&nbsp;</div>
		</div>
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="<?php bloginfo("template_url"); ?>/assets/js/swiper.min.js"></script>
		<script src="<?php bloginfo("template_url"); ?>/assets/js/scripts.js"></script>
	</body>
</html>